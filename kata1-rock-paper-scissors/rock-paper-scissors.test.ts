// The 3 laws of TDD:
//   1. You are not allowed to write any production code unless it is to make a failing unit test pass.
//   2. You are not allowed to write any more of a unit test than is sufficient to fail; and compilation failures are failures.
//   3. You are not allowed to write any more production code than is sufficient to pass the one failing unit test.
// Red -> Green -> Refactor
// Fake It Green Bar Pattern

import { createRockPaperScissors, Move, Outcome } from "./rock-paper-scissors";

describe("rock-paper-scissors", () => {
  describe("play", () => {
    describe("Rock beats scissors", () => {
      test("Player plays rock, given opponent plays scissors, should return 'Player wins'", () => {
        // Arrange
        const sut = createRockPaperScissors();
        const expected = Outcome.PlayerWins;
        const playerMove = Move.Rock;
        const opponentMove = Move.Scissors;

        // Act
        const actual = sut.play(playerMove, opponentMove);

        // Assert
        expect(actual).toBe(expected);
      });

      test("Player plays scissors, given opponent plays rock, should return 'Player loses", () => {
        // Arrange
        const sut = createRockPaperScissors();
        const expected = Outcome.PlayerLoses;
        const playerMove = Move.Scissors;
        const opponentMove = Move.Rock;

        // Act
        const actual = sut.play(playerMove, opponentMove);

        // Assert
        expect(actual).toBe(expected);
      });
    });

    describe("Paper beats rock", () => {
      test.each([
        {
          playerMove: Move.Paper,
          opponentMove: Move.Rock,
          expected: Outcome.PlayerWins,
        },
        {
          playerMove: Move.Rock,
          opponentMove: Move.Paper,
          expected: Outcome.PlayerLoses,
        },
      ])(
        "Player plays $playerMove, given opponent plays $opponentMove, should return $expected",
        ({ playerMove, opponentMove, expected }) => {
          // Arrange
          const sut = createRockPaperScissors();

          // Act
          const actual = sut.play(playerMove, opponentMove);

          // Assert
          expect(actual).toBe(expected);
        }
      );
    });

    describe("Scissors beats paper", () => {
      test.each([
        {
          playerMove: Move.Scissors,
          opponentMove: Move.Paper,
          expected: Outcome.PlayerWins,
        },
        {
          playerMove: Move.Paper,
          opponentMove: Move.Scissors,
          expected: Outcome.PlayerLoses,
        },
      ])(
        "Player plays $playerMove, given opponent plays $opponentMove, should return $expected",
        ({ playerMove, opponentMove, expected }) => {
          // Arrange
          const sut = createRockPaperScissors();

          // Act
          const actual = sut.play(playerMove, opponentMove);

          // Assert
          expect(actual).toBe(expected);
        }
      );
    });

    describe("Spock smashes scissors", () => {
      test.each([
        {
          playerMove: Move.Spock,
          opponentMove: Move.Scissors,
          expected: Outcome.PlayerWins,
        },
        {
          playerMove: Move.Scissors,
          opponentMove: Move.Spock,
          expected: Outcome.PlayerLoses,
        },
      ])(
        "Player plays $playerMove, given opponent plays $opponentMove, should return $expected",
        ({ playerMove, opponentMove, expected }) => {
          // Arrange
          const sut = createRockPaperScissors();

          // Act
          const actual = sut.play(playerMove, opponentMove);

          // Assert
          expect(actual).toBe(expected);
        }
      );
    });

    describe("Rock crushes lizard", () => {
      test.each([
        {
          playerMove: Move.Rock,
          opponentMove: Move.Lizard,
          expected: Outcome.PlayerWins,
        },
        {
          playerMove: Move.Lizard,
          opponentMove: Move.Rock,
          expected: Outcome.PlayerLoses,
        },
      ])(
        "Player plays $playerMove, given opponent plays $opponentMove, should return $expected",
        ({ playerMove, opponentMove, expected }) => {
          // Arrange
          const sut = createRockPaperScissors();

          // Act
          const actual = sut.play(playerMove, opponentMove);

          // Assert
          expect(actual).toBe(expected);
        }
      );
    });

    describe("Lizard poisons Spock", () => {
      test.each([
        {
          playerMove: Move.Lizard,
          opponentMove: Move.Spock,
          expected: Outcome.PlayerWins,
        },
        {
          playerMove: Move.Spock,
          opponentMove: Move.Lizard,
          expected: Outcome.PlayerLoses,
        },
      ])(
        "Player plays $playerMove, given opponent plays $opponentMove, should return $expected",
        ({ playerMove, opponentMove, expected }) => {
          // Arrange
          const sut = createRockPaperScissors();

          // Act
          const actual = sut.play(playerMove, opponentMove);

          // Assert
          expect(actual).toBe(expected);
        }
      );
    });

    describe("Scissors decapitate lizard", () => {
      test.each([
        {
          playerMove: Move.Scissors,
          opponentMove: Move.Lizard,
          expected: Outcome.PlayerWins,
        },
        {
          playerMove: Move.Lizard,
          opponentMove: Move.Scissors,
          expected: Outcome.PlayerLoses,
        },
      ])(
        "Player plays $playerMove, given opponent plays $opponentMove, should return $expected",
        ({ playerMove, opponentMove, expected }) => {
          // Arrange
          const sut = createRockPaperScissors();

          // Act
          const actual = sut.play(playerMove, opponentMove);

          // Assert
          expect(actual).toBe(expected);
        }
      );
    });

    describe("Lizard eats paper", () => {
      test.each([
        {
          playerMove: Move.Lizard,
          opponentMove: Move.Paper,
          expected: Outcome.PlayerWins,
        },
        {
          playerMove: Move.Paper,
          opponentMove: Move.Lizard,
          expected: Outcome.PlayerLoses,
        },
      ])(
        "Player plays $playerMove, given opponent plays $opponentMove, should return $expected",
        ({ playerMove, opponentMove, expected }) => {
          // Arrange
          const sut = createRockPaperScissors();

          // Act
          const actual = sut.play(playerMove, opponentMove);

          // Assert
          expect(actual).toBe(expected);
        }
      );
    });

    describe("Paper disproves spock", () => {
      test.each([
        {
          playerMove: Move.Paper,
          opponentMove: Move.Spock,
          expected: Outcome.PlayerWins,
        },
        {
          playerMove: Move.Spock,
          opponentMove: Move.Paper,
          expected: Outcome.PlayerLoses,
        },
      ])(
        "Player plays $playerMove, given opponent plays $opponentMove, should return $expected",
        ({ playerMove, opponentMove, expected }) => {
          // Arrange
          const sut = createRockPaperScissors();

          // Act
          const actual = sut.play(playerMove, opponentMove);

          // Assert
          expect(actual).toBe(expected);
        }
      );
    });

    describe("Spock vaporizes rock", () => {
      test.each([
        {
          playerMove: Move.Spock,
          opponentMove: Move.Rock,
          expected: Outcome.PlayerWins,
        },
        {
          playerMove: Move.Rock,
          opponentMove: Move.Spock,
          expected: Outcome.PlayerLoses,
        },
      ])(
        "Player plays $playerMove, given opponent plays $opponentMove, should return $expected",
        ({ playerMove, opponentMove, expected }) => {
          // Arrange
          const sut = createRockPaperScissors();

          // Act
          const actual = sut.play(playerMove, opponentMove);

          // Assert
          expect(actual).toBe(expected);
        }
      );
    });

    describe("If both the player and the opponent play the same move, the game is a draw", () => {
      test.each([
        {
          playerMove: Move.Rock,
          opponentMove: Move.Rock,
          expected: Outcome.Draw,
        },
        {
          playerMove: Move.Paper,
          opponentMove: Move.Paper,
          expected: Outcome.Draw,
        },
        {
          playerMove: Move.Scissors,
          opponentMove: Move.Scissors,
          expected: Outcome.Draw,
        },
        {
          playerMove: Move.Spock,
          opponentMove: Move.Spock,
          expected: Outcome.Draw,
        },
        {
          playerMove: Move.Lizard,
          opponentMove: Move.Lizard,
          expected: Outcome.Draw,
        },
      ])(
        "Player plays $playerMove, given opponent plays $opponentMove, should return $expected",
        ({ playerMove, opponentMove, expected }) => {
          // Arrange
          const sut = createRockPaperScissors();

          // Act
          const actual = sut.play(playerMove, opponentMove);

          // Assert
          expect(actual).toBe(expected);
        }
      );
    });
  });
});
