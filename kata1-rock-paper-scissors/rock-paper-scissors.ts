export enum Move {
  Rock = "Rock",
  Paper = "Paper",
  Scissors = "Scissors",
  Spock = "Spock",
  Lizard = "Lizard",
}

export enum Outcome {
  PlayerWins = "Player Wins",
  PlayerLoses = "Player Loses",
  Draw = "Draw",
}

interface RockPaperScissors {
  play(playerMove: Move, opponentMove: Move): Outcome;
}

export function createRockPaperScissors(): RockPaperScissors {
  const scenarios = [
    // Paper beats Rock
    {
      playerMove: Move.Rock,
      opponentMove: Move.Paper,
      outcome: Outcome.PlayerLoses,
    },
    {
      playerMove: Move.Paper,
      opponentMove: Move.Rock,
      outcome: Outcome.PlayerWins,
    },

    // Rock Beats Scissors
    {
      playerMove: Move.Rock,
      opponentMove: Move.Scissors,
      outcome: Outcome.PlayerWins,
    },
    {
      playerMove: Move.Scissors,
      opponentMove: Move.Rock,
      outcome: Outcome.PlayerLoses,
    },

    // Scissors beats Paper
    {
      playerMove: Move.Paper,
      opponentMove: Move.Scissors,
      outcome: Outcome.PlayerLoses,
    },
    {
      playerMove: Move.Scissors,
      opponentMove: Move.Paper,
      outcome: Outcome.PlayerWins,
    },

    // Spock smashes scissors
    {
      playerMove: Move.Scissors,
      opponentMove: Move.Spock,
      outcome: Outcome.PlayerLoses,
    },
    {
      playerMove: Move.Spock,
      opponentMove: Move.Scissors,
      outcome: Outcome.PlayerWins,
    },

    // Rock crushes Lizard
    {
      playerMove: Move.Rock,
      opponentMove: Move.Lizard,
      outcome: Outcome.PlayerWins,
    },
    {
      playerMove: Move.Lizard,
      opponentMove: Move.Rock,
      outcome: Outcome.PlayerLoses,
    },

    // Lizard poisons Spock
    {
      playerMove: Move.Spock,
      opponentMove: Move.Lizard,
      outcome: Outcome.PlayerLoses,
    },
    {
      playerMove: Move.Lizard,
      opponentMove: Move.Spock,
      outcome: Outcome.PlayerWins,
    },

    // Scissors Decapitates Lizard
    {
      playerMove: Move.Scissors,
      opponentMove: Move.Lizard,
      outcome: Outcome.PlayerWins,
    },
    {
      playerMove: Move.Lizard,
      opponentMove: Move.Scissors,
      outcome: Outcome.PlayerLoses,
    },

    // Lizard eats paper
    {
      playerMove: Move.Lizard,
      opponentMove: Move.Paper,
      outcome: Outcome.PlayerWins,
    },
    {
      playerMove: Move.Paper,
      opponentMove: Move.Lizard,
      outcome: Outcome.PlayerLoses,
    },

    // Paper disproves Spock
    {
      playerMove: Move.Paper,
      opponentMove: Move.Spock,
      outcome: Outcome.PlayerWins,
    },
    {
      playerMove: Move.Spock,
      opponentMove: Move.Paper,
      outcome: Outcome.PlayerLoses,
    },

    // Spock vaporizes Rock
    {
      playerMove: Move.Spock,
      opponentMove: Move.Rock,
      outcome: Outcome.PlayerWins,
    },
    {
      playerMove: Move.Rock,
      opponentMove: Move.Spock,
      outcome: Outcome.PlayerLoses,
    },
  ];
  return {
    play(playerMove: Move, opponentMove: Move): Outcome {
      const result = scenarios.find(
        (scenario) =>
          scenario.playerMove === playerMove &&
          scenario.opponentMove === opponentMove
      );

      return result ? result.outcome : Outcome.Draw;
      // return resultMatrix[playerMove][opponentMove];
    },
  };
}
