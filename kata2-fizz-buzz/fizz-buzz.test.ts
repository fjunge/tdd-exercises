// Boundaries and Equivalence Partitions
// Triangulation Green Bar Pattern
// Test Cases (.each())

import { createFizzBuzz } from "./fizz-buzz";

// Also remember:
//  - 3 laws
//  - red-green-refactor
//  - Fake It Green Bar Pattern

// Rules:
//  - Divisible by 3 return Fizz
//  - Divisible by 5 return Buzz
//  - Divisible by 3 and 5 return FizzBuzz
//  - Not divisible by 3 or 5 return the number as a string

describe("fizzBuzz", () => {
  describe("Numbers divisible by 3 (and not divisible by 5), other than the number 3 itself, should return Fizz", () => {
    test.each([{ input: 6 }, { input: 9 }, { input: 12 }])(
      "$input",
      ({ input }) => {
        // Arrange
        const expected = "Fizz";
        const sut = createFizzBuzz();

        // Act
        const actual = sut.go(input);

        // Assert
        expect(actual).toBe(expected);
      }
    );
  });

  describe("Numbers divisible by 5 (and not divisible by 3), other than the number 5 itself, should return Buzz", () => {
    test.each([{ input: 10 }, { input: 20 }, { input: 25 }])(
      "$input",
      ({ input }) => {
        // Arrange
        const expected = "Buzz";
        const sut = createFizzBuzz();

        // Act
        const actual = sut.go(input);

        // Assert
        expect(actual).toBe(expected);
      }
    );
  });

  describe("Numbers divisible by both 5 and 3 should return FizzBuzz", () => {
    test.each([{ input: 15 }, { input: 30 }, { input: 45 }])(
      "$input",
      ({ input }) => {
        // Arrange
        const expected = "FizzBuzz";
        const sut = createFizzBuzz();

        // Act
        const actual = sut.go(input);

        // Assert
        expect(actual).toBe(expected);
      }
    );
  });

  describe("Numbers that are prime should end in Whizz", () => {
    test("The number 3 should return FizzWhizz", () => {
      // Arrange
      const expected = "FizzWhizz";
      const input = 3;
      const sut = createFizzBuzz();

      // Act
      const actual = sut.go(input);

      // Assert
      expect(actual).toBe(expected);
    });

    test("The number 5 should return BuzzWhizz", () => {
      // Arrange
      const expected = "BuzzWhizz";
      const input = 5;
      const sut = createFizzBuzz();

      // Act
      const actual = sut.go(input);

      // Assert
      expect(actual).toBe(expected);
    });

    describe("Other prime numbers should return just Whiz", () => {
      test.each([
        { input: 2 },
        { input: 7 },
        { input: 11 },
        { input: 13 },
        { input: 97 },
      ])("$input", ({ input }) => {
        // Arrange
        const expected = "Whiz";
        const sut = createFizzBuzz();

        // Act
        const actual = sut.go(input);

        // Assert
        expect(actual).toBe(expected);
      });
    });
  });

  describe("All other number should return themselves as a string", () => {
    test.each([
      { input: 1, expected: "1" },
      { input: 4, expected: "4" },
      { input: 8, expected: "8" },
      { input: 14, expected: "14" },
      { input: 76, expected: "76" },
    ])("$input", ({ input, expected }) => {
      // Arrange
      const sut = createFizzBuzz();

      // Act
      const actual = sut.go(input);

      // Assert
      expect(actual).toBe(expected);
    });
  });
});
