interface FizzBuzz {
  go(num: number): string;
}

export function createFizzBuzz(): FizzBuzz {
  function trialDivision(num: number): boolean {
    if (num < 2) {
      return false;
    }
    if (num === 2) {
      return true;
    }
    if (num % 2 === 0) {
      return false;
    }
    for (let i = 3; i * i <= num; i += 2) {
      if (num % i === 0) {
        return false;
      }
    }
    return true;
  }
  return {
    go: function (num: number): string {
      if (num === 3) {
        return "FizzWhizz";
      } else if (num === 5) {
        return "BuzzWhizz";
      } else if (num % 15 === 0) {
        return "FizzBuzz";
      } else if (num % 3 === 0) {
        return "Fizz";
      } else if (num % 5 == 0) {
        return "Buzz";
      }

      return trialDivision(num) ? "Whiz" : num.toString();
    },
  };
}
