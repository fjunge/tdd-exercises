// The stages of naming
//  1. Meaningless
//  2. Accurate
//  3. Precise / Specific
//  4. Meaningful

import { createAgeCalculator } from "./age-calculator";

describe("age calculator", () => {
  describe("Birthday having already taken place in the year", () => {
    describe("Birthday is in earlier month", () => {
      test.each([
        {
          birthDate: "1979/02/15",
          targetDate: "2000/03/21",
          expected: 21,
        },
        {
          birthDate: "1950/01/31",
          targetDate: "2001/03/21",
          expected: 51,
        },
      ])(
        "birthDate: $birthDate, targetDate: $targetDate, expected: $expected",
        ({ birthDate, targetDate, expected }) => {
          // Arrange
          const sut = createAgeCalculator();

          // Act
          const actual = sut(new Date(birthDate), new Date(targetDate));

          // Assert
          expect(actual).toBe(expected);
        }
      );
    });

    describe("Birthday is in same month", () => {
      test.each([
        {
          birthDate: "2015/03/10",
          targetDate: "2022/03/21",
          expected: 7,
        },
        {
          birthDate: "1993/06/24",
          targetDate: "2024/06/26",
          expected: 31,
        },
      ])(
        "birthDate: $birthDate, targetDate: $targetDate, expected: $expected",
        ({ birthDate, targetDate, expected }) => {
          // Arrange
          const sut = createAgeCalculator();

          // Act
          const actual = sut(new Date(birthDate), new Date(targetDate));

          // Assert
          expect(actual).toBe(expected);
        }
      );
    });

    describe("The Year is the same", () => {
      test.each([
        {
          birthDate: "2016/01/31",
          targetDate: "2016/03/21",
          expected: 0,
        },
        {
          birthDate: "1970/08/05",
          targetDate: "1970/12/01",
          expected: 0,
        },
      ])(
        "birthDate: $birthDate, targetDate: $targetDate, expected: $expected",
        ({ birthDate, targetDate, expected }) => {
          // Arrange
          const sut = createAgeCalculator();

          // Act
          const actual = sut(new Date(birthDate), new Date(targetDate));

          // Assert
          expect(actual).toBe(expected);
        }
      );

      describe("and the month is the same", () => {
        test.each([
          {
            birthDate: "2016/01/05",
            targetDate: "2016/01/10",
            expected: 0,
          },
          {
            birthDate: "1960/01/15",
            targetDate: "1960/01/21",
            expected: 0,
          },
        ])(
          "birthDate: $birthDate, targetDate: $targetDate, expected: $expected",
          ({ birthDate, targetDate, expected }) => {
            // Arrange
            const sut = createAgeCalculator();

            // Act
            const actual = sut(new Date(birthDate), new Date(targetDate));

            // Assert
            expect(actual).toBe(expected);
          }
        );
      });
    });
  });
  describe("Birthday having not yet taken place in the year", () => {
    describe("Birthday is in later month", () => {
      test.each([
        {
          birthDate: "1980/02/15",
          targetDate: "2005/01/21",
          expected: 24,
        },
        {
          birthDate: "1953/10/31",
          targetDate: "2004/03/21",
          expected: 50,
        },
      ])(
        "birthDate: $birthDate, targetDate: $targetDate, expected: $expected",
        ({ birthDate, targetDate, expected }) => {
          // Arrange
          const sut = createAgeCalculator();

          // Act
          const actual = sut(new Date(birthDate), new Date(targetDate));

          // Assert
          expect(actual).toBe(expected);
        }
      );
    });
    describe("Birthday is in same month", () => {
      test.each([
        {
          birthDate: "2016/03/31",
          targetDate: "2023/03/21",
          expected: 6,
        },
        {
          birthDate: "1950/01/31",
          targetDate: "2000/01/01",
          expected: 49,
        },
      ])(
        "birthDate: $birthDate, targetDate: $targetDate, expected: $expected",
        ({ birthDate, targetDate, expected }) => {
          // Arrange
          const sut = createAgeCalculator();

          // Act
          const actual = sut(new Date(birthDate), new Date(targetDate));

          // Assert
          expect(actual).toBe(expected);
        }
      );
    });
  });
  describe("Birthday is on same day as target date", () => {
    test.each([
      {
        birthDate: "1980/02/15",
        targetDate: "2005/02/15",
        expected: 25,
      },
      {
        birthDate: "1953/10/31",
        targetDate: "2004/10/31",
        expected: 51,
      },
      {
        birthDate: "2016/03/31",
        targetDate: "2023/03/31",
        expected: 7,
      },
    ])(
      "birthDate: $birthDate, targetDate: $targetDate, expected: $expected",
      ({ birthDate, targetDate, expected }) => {
        // Arrange
        const sut = createAgeCalculator();

        // Act
        const actual = sut(new Date(birthDate), new Date(targetDate));

        // Assert
        expect(actual).toBe(expected);
      }
    );
  });
  describe("The target date is before the birth date", () => {
    describe("The target date is in an earlier month", () => {
      test.each([
        {
          birthDate: "2022/03/15",
          targetDate: "2005/02/15",
        },
        {
          birthDate: "2016/04/17",
          targetDate: "2016/01/15",
        },
      ])(
        "birthDate: $birthDate, targetDate: $targetDate",
        ({ birthDate, targetDate }) => {
          // Arrange
          const expected = RangeError("The birth day is after the target date");
          const sut = createAgeCalculator();

          // Act
          const actual = () => sut(new Date(birthDate), new Date(targetDate));

          // Assert
          expect(actual).toThrow(expected);
        }
      );
    });
    describe("The target date is in the same month", () => {
      test.each([
        {
          birthDate: "2004/10/05",
          targetDate: "2004/10/04",
        },
        {
          birthDate: "2016/03/31",
          targetDate: "2016/03/20",
        },
      ])(
        "birthDate: $birthDate, targetDate: $targetDate",
        ({ birthDate, targetDate }) => {
          // Arrange
          const expected = RangeError("The birth day is after the target date");
          const sut = createAgeCalculator();

          // Act
          const actual = () => sut(new Date(birthDate), new Date(targetDate));

          // Assert
          expect(actual).toThrow(expected);
        }
      );
    });
  });
});
