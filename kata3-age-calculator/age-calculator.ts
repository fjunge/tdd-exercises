export function createAgeCalculator() {
  return function (birthDate: Date, targetDate: Date): number {
    const differenceInYears =
      targetDate.getFullYear() - birthDate.getFullYear();

    if (
      differenceInYears < 0 ||
      (differenceInYears === 0 &&
        (targetDate.getMonth() < birthDate.getMonth() ||
          (targetDate.getMonth() === birthDate.getMonth() &&
            targetDate.getDate() < birthDate.getDate())))
    ) {
      throw RangeError("The birth day is after the target date");
    }

    if (
      targetDate.getMonth() < birthDate.getMonth() ||
      (targetDate.getMonth() === birthDate.getMonth() &&
        targetDate.getDate() < birthDate.getDate())
    ) {
      return differenceInYears - 1;
    }

    return differenceInYears;
  };
}
